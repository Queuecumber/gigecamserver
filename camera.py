from pymba import Vimba
from threading import Thread, Event, RLock

vimba = Vimba()
vimba.startup()

_frame = (None, 0, 0)
_frame_lock = RLock()

_acquisition_event = Event()


class Camera:
    def __init__(self, ip):
        self.stop_grabber = Event()
        self.grabber = Thread(target=_grabber, args=(self.stop_grabber, ip))
        self.grabber.start()
        self.active_clients = 0
        self.active_client_lock = RLock()

    def __del__(self):
        self.stop_grabber.set()
        _acquisition_event.set()

    def begin(self):
        with self.active_client_lock:
            self.active_clients += 1

        _acquisition_event.set()

    def end(self):
        with self.active_client_lock:
            self.active_clients -= 1

            if self.active_clients == 0:
                _acquisition_event.clear()

    def next(self):
        with _frame_lock:
            return _frame


def _grabber(stop_condition, ip):
    global _frame

    camera, frame = start_camera(ip)

    # TODO use a condition variable to sleep unless someone actually wants frames to reduce traffic
    while not stop_condition.is_set():
        _acquisition_event.wait()
        try:
            frame.queueFrameCapture()
        except Exception as e:
            print(e, flush=True)
            continue

        with _frame_lock:
            frame.waitFrameCapture(1000)
            _frame = (frame.getBufferByteData(), frame.height, frame.width)

    destroy_camera(camera)


def start_camera(ip):
    camera = vimba.getCamera(ip)
    camera.openCamera()
    camera.runFeatureCommand('GVSPAdjustPacketSize')

    camera.PixelFormat = 'Mono8'

    camera.BinningHorizontal = 2
    camera.BinningHorizontalMode = 'Average'
    camera.BinningVertical = 2
    camera.BinningVerticalMode = 'Average'

    camera.GainAuto = 'Off'
    camera.Gain = 0

    camera.ExposureAuto = 'Continuous'
    camera.ExposureAutoMax = 5000000
    camera.ExposureAutoTarget = 25

    camera.StreamBytesPerSecond = camera.HeightMax * camera.WidthMax * 10 * 1
    camera.StreamFrameRateConstrain = True

    camera.AcquisitionMode = 'Continuous'
    camera.AcquisitionFrameRate = 10

    frame = camera.getFrame()
    frame.announceFrame()

    camera.startCapture()
    camera.runFeatureCommand('AcquisitionStart')

    return camera, frame


def destroy_camera(camera):
    camera.runFeatureCommand('AcquisitionStop')

    camera.endCapture()
    camera.revokeAllFrames()
    camera.closeCamera()
