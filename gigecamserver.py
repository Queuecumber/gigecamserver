from camera import Camera
from after_response import AfterResponse
from flask import Flask, Response
import os
import io
from PIL import Image

app = Flask(__name__)
AfterResponse(app)


def encode(frame, w, h):
    frame_img = Image.frombuffer('L', (w, h), frame, 'raw', 'L', 0, 1)
    with io.BytesIO() as frame_bytes:
        frame_img.save(frame_bytes, format='JPEG')
        frame_array = frame_bytes.getvalue()

    return frame_array


@app.route('/mjpeg')
def mjpeg():
    camera.begin()

    def gen_frame():
        while True:
            frame, h, w = camera.next()
            if frame is not None:
                frame_array = encode(frame, w, h)
                yield b'--frame\r\nContent-Type: image/jpeg\r\n\r\n' + frame_array + b'\r\n'

    return Response(gen_frame(), mimetype='multipart/x-mixed-replace; boundary=frame')


@app.after_response
def show_teardown():
    camera.end()


if __name__ == '__main__':
    camera_loc = os.environ['CAMERA_LOCATOR']
    camera = Camera(camera_loc)

    port = os.environ['SERVER_PORT'] if 'SERVER_PORT' in os.environ else 80
    app.run(host='0.0.0.0', port=port)

