FROM python:3.7-slim

RUN apt-get update && apt-get install -y build-essential libboost-all-dev unzip

ADD https://www.alliedvision.com/fileadmin/content/software/software/Vimba/Vimba_v2.1.3_Linux.tgz /
RUN tar xf /Vimba_v2.1.3_Linux.tgz && rm Vimba_v2.1.3_Linux.tgz
ENV GENICAM_GENTL64_PATH /Vimba_2_1/VimbaGigETL/CTI/x86_64bit

ADD https://github.com/morefigs/pymba/archive/master.zip /pymba.zip
RUN unzip pymba.zip && rm pymba.zip && cd pymba-master && python setup.py install

RUN mkdir /gigecamserver
WORKDIR /gigecamserver
ADD requirements.txt /gigecamserver/
RUN pip install --requirement requirements.txt

ADD camera.py \
    gigecamserver.py \
    after_response.py \
    /gigecamserver/

CMD ["python", "gigecamserver.py"]